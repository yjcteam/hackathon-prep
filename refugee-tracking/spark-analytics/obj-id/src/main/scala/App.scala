/**
  * #expeditionhacks
  */

import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.spark.SparkConf
import org.apache.spark.storage.StorageLevel

object ObjectIdentification {

  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("ObjectIdentification").setMaster("local[2]").set("spark.executor.memory", "1g")
    val sc = new SparkContext(conf)
    println(sc.version)

    println("ObjectIdentification V0.1")

    // For all object images

    // Match against social media profile

    // Create object graph for queries

    // Store to database
  }
}
