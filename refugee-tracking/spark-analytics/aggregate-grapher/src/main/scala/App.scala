
/**
  * #expeditionhacks
  * This module does the following:
  *  1. Loads frames sent by drone
  *  2. Creates individual images of objects detected
  *  3. Maps it with social media proviles to identify the individual
  *  4. Creates graph people observed in a location
  *
  */

import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.spark.SparkConf
import org.apache.spark.storage.StorageLevel

object AggregateGrapher {

  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("AggregateGrapher").setMaster("local[2]").set("spark.executor.memory", "1g")
    val sc = new SparkContext(conf)

    println(sc.version)

    println("Aggregate Grapher V0.1")

    // Load images received from drones

    // For each image, create seperate image for each object from base image

    // Create graphs of objects for a particular location

    // Store it for visualization

  }
}
