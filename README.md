Feed.Me
==================

This project was created with Currell Berry's rest-start archetype.

About
------------

This project skeleton includes the following features currently:

1. JUnit Tests
2. Embedded Jetty
3. Jersey setup demonstrating simple RESTful web resource.
4. Hibernate setup for Mysql (prod) and Derby (test)
5. Simple Hibernate unit tests demonstrating use with minimal data model.

Setup
------------

### Preconditions
This project uses Apache Maven to manage builds.  You can obtain Apache Maven [here](https://maven.apache.org/).

This project relies on Mysql as well.  Install mysql and create a database named "homelesshelp" within it.

database user as configured currently is "root", database password is "nroot".

## Build/Test
=======
The unit tests can be run with either mysql or derby.  Currently they are configured to run on derby by default.


To build:

    mvn clean package

To run tests only:

    mvn test

