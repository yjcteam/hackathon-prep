'use strict'

/*
 * this file is supposed to be the top-level "controller".
 *
 * principles of this file.
 * 1. UI components activate each other through jquery "events" rather than directly.
 * 2. There is a clean separation of concerns between data methods and UI-manipulation methods (model files contain data methods).
 * 3. We use deferred's and promises to handle javascript's asynchronous nature.
 * 4. Handlebars templating.
 * when possible, route events through url!
 *
 * This file is for global application javascript.
 * Any code relating to a specific feature should go in a relevant folder.
 */

var templates = {};
var router = {"currentHash": null};

router.myroute = function(specified_url) {
    console.log("starting myroute");
    var url = specified_url ? location.url : specified_url;
    console.log("next myroute");

   var locationComponent = berryGetHashStrOnly(url).substring(1);
   var fullHash = berryGetHashStrFull(url);

   console.log("yoyo");
   console.log("path_" + locationComponent);

   if (fullHash !== router.currentHash) {
       var queryParams = berryGetQueryParams(berryGetQueryString(url));
       if (history.pushState) history.pushState(0, 0, url);
       console.log("calling in!!! locationComponent["+locationComponent+"]");
       window["path_"+locationComponent].apply(this, queryParams);
       window.onpopstate = null
   } else {
       console.log("staying put");
   }
}

$(document).ready(function(){
    init();
});

function init() {
    //1. Set up global jquery ajax settings
    $.ajaxSetup({
        headers: {
        Accept: 'application/json',
        Authorization: getAuthString(),
        },
        crossDomain: true
    });

    //2. Set up event handlers
    initializeEventHandlers();

    //3. Set up window size properties
    //setupWindowSizing();

    //4. Set up handleBars
    setupHandlebars();
    loadGlobalTemplates();
    showLoading();
    //5. set up routing
    setupRouting();
    Model.getUserData() //6. load basic data for future reference
        .then(function() {
            console.log("login promise succeeded");
            hideLoading();
        })
        .fail(function(data) {
            console.log("you are not logged in!");
            hideLoading();
            location.hash = "#login";
        })
        .always(function(data) {
            //7. do routing.
            console.log("routing time");
            router.myroute()
            //doNavigation();
        });
}

function showLoading() {
    var html    = templates["loading-template"]({});
    $("#portal").html(html)
}

function hideLoading() {
}

function setupRouting() {
    console.log("adding event listener");
    window.onhashchange = router.myroute;
    window.onpopstate = router.myroute;
    // document.addEventListener("DOMContentLoaded", router.myroute, false);
}

function initializeEventHandlers() {
//    $(document).on("b:submit", doSubmit());
}

function setupWindowSizing() {
    //nothing required at moment
}


function setupHandlebars() {
    var deferred =  $.Deferred();
    //no helpers to register for now.  pass control onwards.

    return deferred.promise();
}

function loadGlobalTemplates() {
    var deferred =  $.Deferred();

    registerTemplate("#loading-template");
    registerTemplate("#entry-template");
    registerTemplate("#login-template");
    registerTemplate("#home-template");
    registerTemplate("#charity-profile-template");
    registerTemplate("#charity-listing-template");

    return deferred.promise();
}

function registerTemplate(idstr) {
    var source   = $(idstr).html();
    var template = Handlebars.compile(source);
    templates[idstr.substring(1)] = template;
    console.log("registered template for idstr["+idstr+"]");
}

function getAuthString() {
    if(!readCookie("password")) {
        $(document).trigger("b:login_redirect");
    }

    //not great security but will do for now
    return "Basic " + btoa(readCookie("username")) + ":" + btoa(readCookie("password"));
}

function path_() {
    path_charityListing();
}

function path_login() {
    var context = {title: "My New Post", body: "This is my first post!"};
    var html    = templates["login-template"](context);
    $("#portal").html(html)

    $("#loginSubmit").submit(function( event ) {
        alert( "Handler for .submit() called." );
        event.preventDefault();
    });

}

function path_myCharityProfile() {
    var context = {title: "My New Post", body: "This is my first post!"};
    var html    = templates["charity-profile-template"](context);
    $("#portal").html(html)
}

function path_charityListing() {
    var context = {title: "My New Post", body: "This is my first post!"};
    var html    = templates["charity-listing-template"](context);
    $("#portal").html(html)
}


