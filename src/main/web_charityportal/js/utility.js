// CB various utility functions


// ********** URL COMPONENTS tools ************
//basic implementation of getting query params -- from https://stackoverflow.com/questions/901115/how-can-i-get-query-string-values-in-javascript
//example: getQueryParams("?mykey=myval")
//example 2: getQueryParams()
function berryGetQueryParams(input) {
    if (!input) {
        input = berryGetQueryString()
    }
    var qd = {}

    input.substr(1).split("&").forEach(
        function(item) {
            var s = item.split("="), k = s[0], v = s[1] && decodeURIComponent(s[1]); (qd[k] = qd[k] || []).push(v)
        })

    return qd;
}

// the standard location.search does not handle hash urls
// this returns the query string along with the leading question mark
function berryGetQueryString(input) {
    if (!input) {
        input = location.href
    }

    console.log("myinput " + input)
    var questionLoc = input.indexOf("?")
    return input.substring(questionLoc)
}

function berryGetHashStrOnly(input) {
    if (!input) {
        input = location.href
    }

    console.log("myinput " + input)
    var hashLoc = input.indexOf("#")
    if (hashLoc === -1) {
        return "";
    }
    var questionLoc = input.indexOf("?")
    if (questionLoc > 0) {
        return input.substring(hashLoc,questionLoc)
    } else {
        return input.substring(hashLoc)
    }
}

function berryGetHashStrFull(input) {
    if (!input) {
        input = location.href
    }

    var hashLoc = input.indexOf("#")
    if (hashLoc === -1) {
        return "";
    }
    var questionLoc = input.indexOf("?")
    return input.substring(hashLoc)
}


// ********** COOKIES tools ************
// https://stackoverflow.com/questions/14573223/set-cookie-and-get-cookie-with-javascript
function createCookie(name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name,"",-1);
}

