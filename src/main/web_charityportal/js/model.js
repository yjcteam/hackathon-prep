var urlRoot = "http://localhost:8080/charityFacade"
var Model = {};

//CB TODO add proper error handling
Model.checkLogin = function() {
   var deferred = $.Deferred();

   $.ajax({
      method: "GET",
      url: urlRoot + "/logincheck"
   }).done(function( obj ) {
       var loggedIn = obj["loggedIn"];
       if (loggedIn == "true") {
         console.log("resolving login promise");
         deferred.resolve();
       } else {
         console.log("rejecting login promise");
         deferred.reject();
       }
   });

   return deferred.promise();
}

Model.getUserData = function() {
   var deferred = $.Deferred();

    return deferred.resolve().promise();
}

Model.login = function() {
   var deferred = $.Deferred();

   $.ajax({
      method: "POST",
      url: urlRoot + "/login",
      data:$("#myForm input").serialize()
   }).done(function( obj ) {
       var loggedIn = obj["loggedIn"];
       if (loggedIn == "true") {
         console.log("resolving login promise");
         deferred.resolve();
       } else {
         console.log("rejecting login promise");
         deferred.reject();
       }
   });

   return deferred.promise();
}

Model.getLocationUpdates = function() {
   var deferred = $.Deferred();

   $.ajax({
      method: "GET",
      url: urlRoot + "/login",
      data:$("#myForm input").serialize()
   }).done(function( obj ) {
       var loggedIn = obj["loggedIn"];
       if (loggedIn == "true") {
         console.log("resolving login promise");
         deferred.resolve();
       } else {
         console.log("rejecting login promise");
         deferred.reject();
       }
   });

   return deferred.promise();
}
