package homelesshelp.facade;


import homelesshelp.model.oms.User;
import org.glassfish.jersey.filter.LoggingFilter;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URISyntaxException;
import java.util.logging.Logger;

@Path("/charityFacade")
public class CharityHelperFacade extends ResourceConfig {

	Logger logger = Logger.getLogger(CharityHelperFacade.class.getName());
	private static final String PERSISTENCE_UNIT_NAME = "homelesshelp_Persistence";
    private EntityManagerFactory factory;

	public CharityHelperFacade() {
		register(LoggingFilter.class);

	    // Enable Tracing support.
        property(ServerProperties.TRACING, "ALL");

        factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
	}

//	@GET
//	@Path("/")
//	@Produces(MediaType.TEXT_PLAIN)
//	public String checkUp() {
//		return "hi";
//	}

	@POST
	@Path("login")
	@Produces(MediaType.APPLICATION_JSON)
	public String authenticate(@FormParam("user") String user, @FormParam("password") String password,
							   @Context HttpServletRequest req) {

        //CB TODO get actual login implementation working!
		if ("superCharity".equals(user) && "pass123".equals(password)) {
			//create an http session and save a cookie on the response
			HttpSession session= req.getSession(true);
			Long foo = (Long) session.getAttribute("userID");
			if (foo != null) {
				logger.info("already logged in");
			} else {
				long userID = 1;
				session.setAttribute("userID",userID);
			}
		} else {
		    throw new WebApplicationException("invalid user/password!", Response.Status.UNAUTHORIZED);
		}

		return "{loggedIn: \"true\"}";
	}

	@GET
	@Path("logincheck")
	@Produces(MediaType.APPLICATION_JSON)
	public String authenticate(@Context HttpServletRequest req) { //check if this session is actively logged in
        Long userID = (Long) req.getSession(true).getAttribute("userID");
        if (userID == null) { //then we are not logged in!
			return "{\"loggedIn\": \"false\"}";
		}

		return "{\"loggedIn\": \"true\"}";
	}

	@POST
	@Path("logout")
	@Produces(MediaType.APPLICATION_JSON)
	public void logout(@Context HttpServletRequest req) { //logout the user if he is logged in
        req.getSession().invalidate();;
	}

	//CB this should be POST but get is easier for now
	@GET
	@Path("register")
	public Response register(@Context HttpServletRequest req) throws URISyntaxException {
		User user = new User();
		user.setName(req.getParameter("charity_name"));
		String locationStr= req.getParameter("location_str");
		if (locationStr != null) {
			String[] splitted = locationStr.split(",");
			double latitude = Double.parseDouble(splitted[0]);
			double longitude = Double.parseDouble(splitted[1]);
			user.setLongitude(latitude);
			user.setLongitude(longitude);
		}
		user.setLatitude(Double.parseDouble(req.getParameter("lat")));
		user.setLongitude(Double.parseDouble(req.getParameter("long")));
		user.setBio(req.getParameter("bio"));
		user.setDonationRequest(req.getParameter("donation_request"));
		user.setTotalCapacity(Integer.parseInt(req.getParameter("total_capacity")));
		user.setAvailableCapacity(Integer.parseInt(req.getParameter("available_capacity")));

		EntityManager em = factory.createEntityManager();
		em.getTransaction().begin();
		try {
			em.persist(user);
			em.getTransaction().commit();

		} finally {
			em.close();
		}

	    java.net.URI location = new java.net.URI("http://localhost:8000/aftersubmit.html");
    	return Response.temporaryRedirect(location).build();
	}

}
