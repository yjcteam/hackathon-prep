package homelesshelp.facade;

import homelesshelp.model.CharityHelperLogic;
import homelesshelp.model.Model;
import homelesshelp.model.oms.User;
import org.glassfish.jersey.filter.LoggingFilter;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import homelesshelp.model.StateLevelDataLogic;
import homelesshelp.model.beans.StateDataBean;;

@Path("/")
public class Facade extends ResourceConfig {

    public Facade() {
		register(LoggingFilter.class);

	    // Enable Tracing support.
        property(ServerProperties.TRACING, "ALL");
	}

	@GET
	@Path("echo")
	@Produces(MediaType.TEXT_PLAIN)
	public String test(@QueryParam("text") String input) {
		return String.format("you sent '%s'\n",input);
	}

    @GET
	@Path("nextPrime")
	@Produces(MediaType.TEXT_PLAIN)
	public String test(@QueryParam("startNum") int startNum) {
        return Integer.toString(Model.findNextBiggerPrime(startNum));
	}

	//simple data of returning json
	@GET
	@Path("mydata")
	@Produces(MediaType.APPLICATION_JSON)
	public Foo mydata(@QueryParam("name") @DefaultValue("jack") String name,
					  @QueryParam("age") @DefaultValue("7") int age) {
		return new Foo(name,age);
	}
	
	//simple data of returning json
	@GET
	@Path("fiftyStates")
	@Produces(MediaType.APPLICATION_JSON)
	public List<StateDataBean> fiftyStates() {
		return StateLevelDataLogic.getFiftyStatesData();
	}	

	public static class Foo {
    	public String name;
    	public int age;

		public Foo(String name, int age) {
			this.name = name;
			this.age = age;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public int getAge() {
			return age;
		}

		public void setAge(int age) {
			this.age = age;
		}
	}

	@GET
	@Path("registeredCharities")
	@Produces(MediaType.APPLICATION_JSON)
	public List<User> registeredCharities() {
		return CharityHelperLogic.getInstance().getRegisteredCharities();
	}

}
