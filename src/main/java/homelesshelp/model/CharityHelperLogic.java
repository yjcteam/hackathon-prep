package homelesshelp.model;

import homelesshelp.facade.CharityHelperFacade;
import homelesshelp.model.oms.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.List;

public class CharityHelperLogic {


    private static final String PERSISTENCE_UNIT_NAME = "homelesshelp_Persistence";
    private static CharityHelperLogic instance;
    private EntityManagerFactory factory;

    private CharityHelperLogic() {
        factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);

    }

    public static CharityHelperLogic getInstance() {
        if (instance == null) {
            instance = new CharityHelperLogic();
        }
        return instance;
    }

    public List<User> getRegisteredCharities() {
        EntityManager em = factory.createEntityManager();
        em.getTransaction().begin();
        try {
        Query q = em.createQuery("select u from User u");
        List<User> out = q.getResultList();
        return q.getResultList();
		} finally {
			em.close();
		}
	}


}
