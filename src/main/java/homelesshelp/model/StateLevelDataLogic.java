package homelesshelp.model;

import java.util.ArrayList;
import java.util.List;
import homelesshelp.model.beans.*;

public class StateLevelDataLogic {
	public static List<StateDataBean> getFiftyStatesData(){
		ArrayList<StateDataBean> stateList = new ArrayList<StateDataBean>();

		stateList.add(new StateDataBean("Alabama", 32.806671, -86.791130, 4689, 9.70));
		stateList.add(new StateDataBean("Alaska", 61.370716, -152.404419, 1946, 26.47));
		stateList.add(new StateDataBean("Arizona", 33.729759, -111.431221, 10562, 15.94));
		stateList.add(new StateDataBean("Arkansas", 34.969704, -92.373123, 3812, 12.88));
		stateList.add(new StateDataBean("California", 37.7749, -122.4194, 136826, 35.69));
		stateList.add(new StateDataBean("California", 36.116203, -119.681564, 136826, 35.69));
		stateList.add(new StateDataBean("Colorado", 39.059811, -105.311104, 9754, 18.51));
		stateList.add(new StateDataBean("Connecticut", 41.597782, -72.755371, 4448, 12.37));
		stateList.add(new StateDataBean("Delaware", 39.318523, -75.507141, 946, 10.22));
		stateList.add(new StateDataBean("DC", 38.897438, -77.026817, 6865, 106.20));
		stateList.add(new StateDataBean("Florida", 27.766279, -81.686783, 47862, 24.48));
		stateList.add(new StateDataBean("Georgia", 33.040619, -83.643074, 16971, 16.98));
		stateList.add(new StateDataBean("Hawaii", 21.094318, -157.498337, 6335, 45.12));
		stateList.add(new StateDataBean("Idaho", 44.240459, -114.478828, 1781, 11.05));
		stateList.add(new StateDataBean("Illinois", 40.349457, -88.986137, 13425, 10.42));
		stateList.add(new StateDataBean("Indiana", 39.849426, -86.258278, 6096, 9.28));
		stateList.add(new StateDataBean("Iowa", 42.011539, -93.210526, 3084, 9.98));
		stateList.add(new StateDataBean("Kansas", 38.526600, -96.726486, 2693, 9.31));
		stateList.add(new StateDataBean("Kentucky", 37.668140, -84.670067, 5245, 11.93));
		stateList.add(new StateDataBean("Louisiana", 31.169546, -91.867805, 5226, 11.30));
		stateList.add(new StateDataBean("Maine", 44.693947, -69.381927, 3016, 22.71));
		stateList.add(new StateDataBean("Maryland", 39.063946, -76.802101, 8205, 13.84));
		stateList.add(new StateDataBean("Pennsylvania", 41.2033, -77.1945, 15086, 11.81));
		stateList.add(new StateDataBean("Texas", 30.2672,-97.7431, 29615, 11.20));

/*
Massachusetts	42.230171	-71.530106
Michigan	43.326618	-84.536095
Minnesota	45.694454	-93.900192
Mississippi	32.741646	-89.678696
Missouri	38.456085	-92.288368
Montana	46.921925	-110.454353
Nebraska	41.125370	-98.268082
Nevada	38.313515	-117.055374
New Hampshire	43.452492	-71.563896
New Jersey	40.298904	-74.521011
New Mexico	34.840515	-106.248482
New York	42.165726	-74.948051
North Carolina	35.630066	-79.806419
North Dakota	47.528912	-99.784012
Ohio	40.388783	-82.764915
Oklahoma	35.565342	-96.928917
Oregon	44.572021	-122.070938
Pennsylvania	40.590752	-77.209755
Rhode Island	41.680893	-71.511780
South Carolina	33.856892	-80.945007
South Dakota	44.299782	-99.438828
Tennessee	35.747845	-86.692345
Texas	31.054487	-97.563461
Utah	40.150032	-111.862434
Vermont	44.045876	-72.710686
Virginia	37.769337	-78.169968
Washington	47.400902	-121.490494
West Virginia	38.491226	-80.954453
Wisconsin	44.268543	-89.616508
Wyoming	42.755966	-107.302490
*/
		
		return stateList;
		
	}
	
	
	
	
}
