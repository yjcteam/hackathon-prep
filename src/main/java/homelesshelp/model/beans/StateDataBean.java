package homelesshelp.model.beans;

public class StateDataBean {
	private String name;
	private Double longitude;
	private Double latitude;
	private int homeless;
	private double rate;
	
	
	public StateDataBean(String name, Double latitude, Double longitude, int homeless) {
		this.name = name;
		this.longitude = longitude;
		this.latitude = latitude;
		this.homeless = homeless;
	}
	
	public StateDataBean(String name, Double latitude, Double longitude, int homeless, double rate) {
		this.name = name;
		this.longitude = longitude;
		this.latitude = latitude;
		this.homeless = homeless;
		this.rate = rate;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public int getHomeless() {
		return homeless;
	}
	public void setHomeless(int homeless) {
		this.homeless = homeless;
	}
	public double getRate() {
		return rate;
	}
	public void setRate(double rate) {
		this.rate = rate;
	}
}
