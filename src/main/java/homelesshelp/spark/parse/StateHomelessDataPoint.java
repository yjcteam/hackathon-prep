package homelesshelp.spark.parse;

// homelesshelp.spark.parse.StateHomelessDataPoint
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class StateHomelessDataPoint {
	
	
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    
	private String state;
	private long totalHomelessPersons;
	// rate per 10,000
	private BigDecimal rate;
	
	public StateHomelessDataPoint() {
		
	}
	
	
	public StateHomelessDataPoint(String state, long totalHomelessPersons, BigDecimal rate) {
		
		this.state = state;
		this.totalHomelessPersons = totalHomelessPersons;
		this.rate = rate;
	}
	
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public long getTotalHomelessPersons() {
		return totalHomelessPersons;
	}
	public void setTotalHomelessPersons(long totalHomelessPersons) {
		this.totalHomelessPersons = totalHomelessPersons;
	}
	public BigDecimal getRate() {
		return rate;
	}
	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}
	
	public String toString() {
		return "state [" + state + "] Total Homeless Persons [" + totalHomelessPersons + "] + rate per 1000 [" + rate + "] ";
	}

}
