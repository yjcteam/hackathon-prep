package homelesshelp.spark.parse;

import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.ParseException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;


import com.opencsv.CSVReader;

public class ParseCSVFiles {

    public static void main(String[] args) throws ParseException {
    	
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("homelesshelp_Persistence");
        EntityManager em = factory.createEntityManager();
        String csvFile = "C:\\hackathon\\2013USStateStatistics.csv";

        CSVReader reader = null;
        try {
            reader = new CSVReader(new FileReader(csvFile));
            String[] line;
            int count = 0;
 
            em.getTransaction().begin();
            
            while ((line = reader.readNext()) != null) {
            	
            	if (count == 0) {
            		count++;
            		continue;
            	}
            	
            	String state = line[0];
            	Long totalHomeless = (Long) NumberFormat.getNumberInstance(java.util.Locale.US).parse((line[1]));
            	BigDecimal rate = new BigDecimal(line[2]);
            	
            	StateHomelessDataPoint dp = 
            			new StateHomelessDataPoint(state, totalHomeless, rate);
            	em.persist(dp);
            	System.out.println(dp.toString());
//                System.out.println("State [id= " + line[0] + ", Total Homeless Persons= " + line[1] 
//                		+ " , Rate of Homeless Population per 10,000" + "= " + line[2] + "]");
            }
    

            // Commit the transaction, which will cause the entity to
            // be stored in the database
            em.getTransaction().commit();

            // It is always good practice to close the EntityManager so that
            // resources are conserved.
            em.close();
    
        } 
        catch (IOException e) {
            e.printStackTrace();
        }


    }
	
}
