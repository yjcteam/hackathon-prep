package homelesshelp;

import homelesshelp.facade.CORSFilter;
import homelesshelp.facade.CharityHelperFacade;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import homelesshelp.facade.Facade;

/**
 * Hello Jersey+Jetty!
 *
 * derived from https://nikolaygrozev.wordpress.com/2014/10/16/rest-with-embedded-jetty-and-jersey-in-a-single-jar-step-by-step/
 *
 */
public class App 
{
    public static void main(String[] args) throws Exception {
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/");
 
        Server jettyServer = new Server(8080);
        jettyServer.setHandler(context);
 
        ServletHolder jerseyServlet = context.addServlet(
             org.glassfish.jersey.servlet.ServletContainer.class, "/*");
        jerseyServlet.setInitOrder(0);
 
        // Tells the Jersey Servlet which REST service/class to load.
        jerseyServlet.setInitParameter(
           "jersey.config.server.provider.classnames",
           Facade.class.getCanonicalName() + ";" +
                   CORSFilter.class.getCanonicalName() + ";" +
                   CharityHelperFacade.class.getCanonicalName() + ";" +
                org.glassfish.jersey.filter.LoggingFilter.class.getCanonicalName()
               );

        jerseyServlet.setInitParameter(
                "jersey.config.server.provider.packages", "com.fasterxml.jackson.jaxrs");

        try {
            jettyServer.start();
            jettyServer.join();
        } finally {
            jettyServer.destroy();
        }
    }
}

