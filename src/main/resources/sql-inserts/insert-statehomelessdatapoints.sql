/*
-- Query: select * from statehomelessdatapoint
LIMIT 0, 1000

-- Date: 2017-07-30 11:28
*/
INSERT INTO `statehomelessdatapoint` (`id`,`rate`,`state`,`totalHomelessPersons`) VALUES (1,9.70,'Alabama',4689);
INSERT INTO `statehomelessdatapoint` (`id`,`rate`,`state`,`totalHomelessPersons`) VALUES (2,26.47,'Alaska',1946);
INSERT INTO `statehomelessdatapoint` (`id`,`rate`,`state`,`totalHomelessPersons`) VALUES (3,15.94,'Arizona',10562);
INSERT INTO `statehomelessdatapoint` (`id`,`rate`,`state`,`totalHomelessPersons`) VALUES (4,12.88,'Arkansas',3812);
INSERT INTO `statehomelessdatapoint` (`id`,`rate`,`state`,`totalHomelessPersons`) VALUES (5,35.69,'California',136826);
INSERT INTO `statehomelessdatapoint` (`id`,`rate`,`state`,`totalHomelessPersons`) VALUES (6,18.51,'Colorado',9754);
INSERT INTO `statehomelessdatapoint` (`id`,`rate`,`state`,`totalHomelessPersons`) VALUES (7,12.37,'Connecticut',4448);
INSERT INTO `statehomelessdatapoint` (`id`,`rate`,`state`,`totalHomelessPersons`) VALUES (8,10.22,'Delaware',946);
INSERT INTO `statehomelessdatapoint` (`id`,`rate`,`state`,`totalHomelessPersons`) VALUES (9,106.20,'District of Columbia',6865);
INSERT INTO `statehomelessdatapoint` (`id`,`rate`,`state`,`totalHomelessPersons`) VALUES (10,24.48,'Florida',47862);
INSERT INTO `statehomelessdatapoint` (`id`,`rate`,`state`,`totalHomelessPersons`) VALUES (11,16.98,'Georgia',16971);
INSERT INTO `statehomelessdatapoint` (`id`,`rate`,`state`,`totalHomelessPersons`) VALUES (12,79.76,'Guam',1271);
INSERT INTO `statehomelessdatapoint` (`id`,`rate`,`state`,`totalHomelessPersons`) VALUES (13,45.12,'Hawaii',6335);
INSERT INTO `statehomelessdatapoint` (`id`,`rate`,`state`,`totalHomelessPersons`) VALUES (14,11.05,'Idaho',1781);
INSERT INTO `statehomelessdatapoint` (`id`,`rate`,`state`,`totalHomelessPersons`) VALUES (15,10.42,'Illinois',13425);
INSERT INTO `statehomelessdatapoint` (`id`,`rate`,`state`,`totalHomelessPersons`) VALUES (16,9.28,'Indiana',6096);
INSERT INTO `statehomelessdatapoint` (`id`,`rate`,`state`,`totalHomelessPersons`) VALUES (17,9.98,'Iowa',3084);
INSERT INTO `statehomelessdatapoint` (`id`,`rate`,`state`,`totalHomelessPersons`) VALUES (18,9.31,'Kansas',2693);
INSERT INTO `statehomelessdatapoint` (`id`,`rate`,`state`,`totalHomelessPersons`) VALUES (19,11.93,'Kentucky',5245);
INSERT INTO `statehomelessdatapoint` (`id`,`rate`,`state`,`totalHomelessPersons`) VALUES (20,11.30,'Louisiana',5226);
INSERT INTO `statehomelessdatapoint` (`id`,`rate`,`state`,`totalHomelessPersons`) VALUES (21,22.71,'Maine',3016);
INSERT INTO `statehomelessdatapoint` (`id`,`rate`,`state`,`totalHomelessPersons`) VALUES (22,13.84,'Maryland',8205);
INSERT INTO `statehomelessdatapoint` (`id`,`rate`,`state`,`totalHomelessPersons`) VALUES (23,28.43,'Massachusetts',19029);
INSERT INTO `statehomelessdatapoint` (`id`,`rate`,`state`,`totalHomelessPersons`) VALUES (24,11.65,'Michigan',11527);
INSERT INTO `statehomelessdatapoint` (`id`,`rate`,`state`,`totalHomelessPersons`) VALUES (25,15.15,'Minnesota',8214);
INSERT INTO `statehomelessdatapoint` (`id`,`rate`,`state`,`totalHomelessPersons`) VALUES (26,8.03,'Mississippi',2403);
INSERT INTO `statehomelessdatapoint` (`id`,`rate`,`state`,`totalHomelessPersons`) VALUES (27,14.20,'Missouri',8581);
INSERT INTO `statehomelessdatapoint` (`id`,`rate`,`state`,`totalHomelessPersons`) VALUES (28,18.50,'Montana',1878);
INSERT INTO `statehomelessdatapoint` (`id`,`rate`,`state`,`totalHomelessPersons`) VALUES (29,16.83,'Nebraska',3145);
INSERT INTO `statehomelessdatapoint` (`id`,`rate`,`state`,`totalHomelessPersons`) VALUES (30,30.26,'Nevada',8443);
INSERT INTO `statehomelessdatapoint` (`id`,`rate`,`state`,`totalHomelessPersons`) VALUES (31,10.93,'New Hampshire',1447);
INSERT INTO `statehomelessdatapoint` (`id`,`rate`,`state`,`totalHomelessPersons`) VALUES (32,13.49,'New Jersey',12002);
INSERT INTO `statehomelessdatapoint` (`id`,`rate`,`state`,`totalHomelessPersons`) VALUES (33,13.52,'New Mexico',2819);
INSERT INTO `statehomelessdatapoint` (`id`,`rate`,`state`,`totalHomelessPersons`) VALUES (34,39.40,'New York',77430);
INSERT INTO `statehomelessdatapoint` (`id`,`rate`,`state`,`totalHomelessPersons`) VALUES (35,12.36,'North Carolina',12168);
INSERT INTO `statehomelessdatapoint` (`id`,`rate`,`state`,`totalHomelessPersons`) VALUES (36,28.60,'North Dakota',2069);
INSERT INTO `statehomelessdatapoint` (`id`,`rate`,`state`,`totalHomelessPersons`) VALUES (37,10.65,'Ohio',12325);
INSERT INTO `statehomelessdatapoint` (`id`,`rate`,`state`,`totalHomelessPersons`) VALUES (38,11.45,'Oklahoma',4408);
INSERT INTO `statehomelessdatapoint` (`id`,`rate`,`state`,`totalHomelessPersons`) VALUES (39,35.17,'Oregon',13822);
INSERT INTO `statehomelessdatapoint` (`id`,`rate`,`state`,`totalHomelessPersons`) VALUES (40,11.81,'Pennsylvania',15086);
INSERT INTO `statehomelessdatapoint` (`id`,`rate`,`state`,`totalHomelessPersons`) VALUES (41,11.42,'Puerto Rico',4128);
INSERT INTO `statehomelessdatapoint` (`id`,`rate`,`state`,`totalHomelessPersons`) VALUES (42,13.16,'Rhode Island',1384);
INSERT INTO `statehomelessdatapoint` (`id`,`rate`,`state`,`totalHomelessPersons`) VALUES (43,13.71,'South Carolina',6544);
INSERT INTO `statehomelessdatapoint` (`id`,`rate`,`state`,`totalHomelessPersons`) VALUES (44,12.95,'South Dakota',1094);
INSERT INTO `statehomelessdatapoint` (`id`,`rate`,`state`,`totalHomelessPersons`) VALUES (45,14.67,'Tennessee',9528);
INSERT INTO `statehomelessdatapoint` (`id`,`rate`,`state`,`totalHomelessPersons`) VALUES (46,11.20,'Texas',29615);
INSERT INTO `statehomelessdatapoint` (`id`,`rate`,`state`,`totalHomelessPersons`) VALUES (47,45.30,'US Virgin Islands',482);
INSERT INTO `statehomelessdatapoint` (`id`,`rate`,`state`,`totalHomelessPersons`) VALUES (48,11.30,'Utah',3277);
INSERT INTO `statehomelessdatapoint` (`id`,`rate`,`state`,`totalHomelessPersons`) VALUES (49,23.20,'Vermont',1454);
INSERT INTO `statehomelessdatapoint` (`id`,`rate`,`state`,`totalHomelessPersons`) VALUES (50,9.23,'Virginia',7625);
INSERT INTO `statehomelessdatapoint` (`id`,`rate`,`state`,`totalHomelessPersons`) VALUES (51,25.48,'Washington',17760);
INSERT INTO `statehomelessdatapoint` (`id`,`rate`,`state`,`totalHomelessPersons`) VALUES (52,12.08,'West Virginia',2240);
INSERT INTO `statehomelessdatapoint` (`id`,`rate`,`state`,`totalHomelessPersons`) VALUES (53,10.63,'Wisconsin',6104);
INSERT INTO `statehomelessdatapoint` (`id`,`rate`,`state`,`totalHomelessPersons`) VALUES (54,16.36,'Wyoming',953);
