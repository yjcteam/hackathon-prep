package homelesshelp;

import org.junit.*;
import homelesshelp.model.Model;

import static org.junit.Assert.*;

/**
 * I usually primarily test the business logic layer.
 */
public class AppTest {

	@Test
    public void testNextPrime()
    {
        assertEquals(Model.findNextBiggerPrime(3),5);
        assertEquals(Model.findNextBiggerPrime(1),2);
        assertEquals(Model.findNextBiggerPrime(960),967);
    }
}
