package homelesshelp;


import static org.junit.Assert.assertTrue;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.junit.Before;
import org.junit.Test;
import homelesshelp.model.oms.Recipe;
import homelesshelp.model.oms.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * based on http://www.vogella.com/tutorials/JavaPersistenceAPI/article.html
 */

public class JpaBasicTest {

//    private static final String PERSISTENCE_UNIT_NAME = "homelesshelp_PersistenceTest";
//    private EntityManagerFactory factory;
//
//    @Before
//    public void setUp() throws Exception {
//        factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
//        EntityManager em = factory.createEntityManager();
//
//        // Begin a new local transaction so that we can persist a new entity
//        em.getTransaction().begin();
//
//        // read the existing entries
//        Query q = em.createQuery("select u from User u");
//        // Persons should be empty
//
//        // do we have entries?
//        boolean createNewEntries = (q.getResultList().size() == 0);
//
//        // No, so lets create new entries
//        if (createNewEntries) {
//            assertTrue(q.getResultList().size() == 0);
//
//            List<Recipe> recipes = new ArrayList<Recipe>();
//
//            for (int i = 0; i < 10; i++) {
//                Recipe recipe = new Recipe();
//                recipe.setTitle("GoodFood " + i);
//                recipe.setBody("Longform Description " + i);
//                em.persist(recipe);
//                recipes.add(recipe);
//            }
//
//            Random random = new Random();
//            for (int i = 0; i < 40; i++) {
//                User person = new User();
//                person.setFirstName("Jim_" + i);
//                person.setLastName("Knopf_" + i);
//                em.persist(person);
//
//                // now persists the recipe person relationship
//                for (int i2 = 0; i2 < 3; i2++) {
//                    int recipeIndex = random.nextInt(recipes.size());
//                    Recipe chosen = recipes.get(recipeIndex);
//                    person.getRecipes().add(chosen);
//                }
//            }
//        }
//
//        // Commit the transaction, which will cause the entity to
//        // be stored in the database
//        em.getTransaction().commit();
//
//        // It is always good practice to close the EntityManager so that
//        // resources are conserved.
//        em.close();
//
//    }
//
//    @Test
//    public void checkAvailablePeople() {
//
//        // now lets check the database and see if the created entries are there
//        // create a fresh, new EntityManager
//        EntityManager em = factory.createEntityManager();
//
//        // Perform a simple query for all the Message entities
//        Query q = em.createQuery("select u from User u");
//
//        // We should have 40 Persons in the database
//        assertTrue(q.getResultList().size() == 40);
//
//        em.close();
//    }
//
//    @Test
//    public void checkRecipesCreated() {
//        EntityManager em = factory.createEntityManager();
//        // Go through each of the entities and print out each of their
//        // messages, as well as the date on which it was created
//        Query q = em.createQuery("select distinct r from Recipe r join r.users u");
//
//        assertTrue(q.getResultList().size() == 10);
//        Recipe r1 = (Recipe) q.getResultList().get(0);
//        assertTrue(r1.getUsers().size() > 0);
//        em.close();
//    }
//
//    @Test(expected = javax.persistence.NoResultException.class)
//    public void deletePerson() {
//        EntityManager em = factory.createEntityManager();
//        // Begin a new local transaction so that we can persist a new entity
//        em.getTransaction().begin();
//        Query q = em
//                .createQuery("SELECT u FROM User u WHERE u.firstName = :firstName AND u.lastName = :lastName");
//        q.setParameter("firstName", "Jim_1");
//        q.setParameter("lastName", "Knopf_!");
//        User user = (User) q.getSingleResult();
//        em.remove(user);
//        em.getTransaction().commit();
//        User person = (User) q.getSingleResult();
//        // Begin a new local transaction so that we can persist a new entity
//
//        em.close();
//    }
}

